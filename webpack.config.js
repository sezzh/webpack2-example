const path = require('path')

module.exports = {
  entry: './src/js/index',
  output: {
    filename: 'app.bundle.js',
    path: path.resolve(__dirname, 'dist', 'js')
  }
}


/*
const path = require('path')

module.exports = {
  entry: './src/js/index',
  output: {
    path: path.resolve(__dirname, 'dist', 'js'),
    filename:: 'app.bundle.js'
  },
  module: {
    rules: [
      { test: [/\.js$/, /\.jsx$/], use: 'babel-loader' }
    ]
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin(),
    new HtmlWebpackPlugin({ template: './src/html/index.html' })
  ]
}
*/
